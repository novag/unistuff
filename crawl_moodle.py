#!/bin/python3

"""
args: <USER> <PASSWORD> [<PATH>]

<PATH> will be created if it does not exist yet.
Directory structure: <PATH>/{COURSE_NAME}(/{SHEET_NAME})/[{FILE_NAME}]

"""

import argparse
import os
import re
import requests
import sys
import lxml.html
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from httplib2 import Http
from oauth2client import file, client, tools

COURSES = [
    {
        'name': 'analysis',
        'id': '43628',
        'splash': False,
        'pattern': r'^(Übungsblatt \d+)$',
        'script': r'^(Skript \d+.\d+.)',
        'comment': ''
    },
    {
        'name': 'gbs',
        'id': '42019',
        'splash': True,
        'pattern': r'^(Tutorium \d\d?)',
        'script': r'^(Folien \d\d: .*)',
        'comment': ''
    },
    {
        'name': 'eidi2',
        'id': '44932',
        'splash': True,
        'pattern': r'^(Exercise\d\d)',
        'comment': ''
    }
]
MOODLE_HOST = 'https://www.moodle.tum.de'
TUMIDP_HOST = 'https://tumidp.lrz.de'
SHIBBOLETH = 'Shibboleth.sso/Login?providerId=https%3A%2F%2Ftumidp.lrz.de%2Fidp%2Fshibboleth&target=https%3A%2F%2Fwww.moodle.tum.de%2Fauth%2Fshibboleth%2Findex.php'
DRIVE_SCOPE = 'https://www.googleapis.com/auth/drive'

parser = tools.argparser
parser.add_argument('-u', '--user', dest='user', type=str, required=True)
parser.add_argument('-p', '--password', dest='password', type=str, required=True)
parser.add_argument('--path', dest='path', type=str)
parser.add_argument('-d', '--drive', dest='drive', action='store_true')
parser.add_argument('-dfi', '--drive-folder-id', dest='drive_folder_id', type=str, default='root')
args = parser.parse_args()

if args.drive:
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', DRIVE_SCOPE)
        creds = tools.run_flow(flow, store)
    drive_service = build('drive', 'v3', http=creds.authorize(Http()))

basepath = os.path.dirname(os.path.realpath(__file__))
if args.path is not None:
    basepath = args.path

umlaut_dict = { 'ä': 'ae', 'ö': 'oe', 'ü': 'ue' }
umlaut_map = { ord(key):val for key, val in umlaut_dict.items() }

session = requests.Session()

def drive_create_folder(name, parent):
    file_metadata = {
        'name': name,
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': [parent]
    }

    try:
        file = drive_service.files().create(body=file_metadata, fields='id').execute()
    except:
        print('Error creating drive folder: {}'.format(name))
        return None

    return file.get('id')

def drive_exists(name, parent):
    response = drive_service.files().list(q="name='{}' and '{}' in parents and trashed=false".format(name, parent),
                                          spaces='drive',
                                          fields='files(id)').execute()

    files = response.get('files', [])

    if files:
        return files[0]['id']

    return False

def upload_drive(filepath, course, folder='assignments'):
    if not args.drive or os.path.splitext(filepath)[1] != '.pdf':
        return False

    filename = os.path.basename(filepath)

    parent = drive_exists(course, args.drive_folder_id)
    if not parent:
        parent = drive_create_folder(course, args.drive_folder_id)

    temp = drive_exists(folder, parent)
    if temp:
        parent = temp
    else:
        parent = drive_create_folder(folder, parent)

    file_metadata = {
        'name': filename,
        'parents': [parent]
    }

    media = MediaFileUpload(filepath, mimetype='application/pdf', resumable=True)
    try:
        file = drive_service.files().create(body=file_metadata, media_body=media).execute()
        print(file)
    except:
        print('Error uploading file: {}'.format(filename))
        return False

    return True

def download_file(url, course, sheet='', folder='assignments'):
    res = session.head(url, allow_redirects=True)
    if not res:
        return None

    size = int(res.headers['content-length'])

    res = re.search(r'filename="(.*)"', res.headers['content-disposition'])
    if not res:
        return None

    filename = res.group(1)
    dirpath = os.path.join(basepath, course, folder, sheet)
    filepath = os.path.join(dirpath, filename)

    if not os.path.exists(dirpath):
        os.makedirs(dirpath)

    if os.path.isfile(filepath):
        if folder != 'scripts' or size <= os.path.getsize(filepath):
            return False

    res = session.get(url, stream=True)
    if not res.ok:
        return None

    with open(filepath, 'wb') as f:
        for chunk in res:
            f.write(chunk)

    return filepath

def regex_xpath(node, path):
    return node.xpath(path, namespaces={"re": "http://exslt.org/regular-expressions"})

def get(path):
    return session.get('{}/{}'.format(MOODLE_HOST, path))

def login():
    html = get(SHIBBOLETH).text
    root = lxml.html.fromstring(html.encode('utf-8'))

    post_url = root.xpath('//form/@action')[0]
    post_data = {
        'j_username': args.user,
        'j_password': args.password,
        'donotcache': 1,
        '_eventId_proceed': ''
    }

    html = session.post('{}/{}'.format(TUMIDP_HOST, post_url), data=post_data).text
    root = lxml.html.fromstring(html.encode('utf-8'))

    post_url = root.xpath('//form/@action')[0]
    post_relay_state = root.xpath('//input[@name="RelayState"]/@value')
    post_saml_response = root.xpath('//input[@name="SAMLResponse"]/@value')
    post_data = {
        'RelayState': post_relay_state,
        'SAMLResponse': post_saml_response
    }

    session.post(post_url, data=post_data)

def fetch_assignment(course_name, sheet, url):
    html = session.get(url).text
    root = lxml.html.fromstring(html.encode('utf-8'))
    files = root.xpath('//div[starts-with(@id,"assign_files_")]//a/@href')
    for file in files:
        if '/submission_files/' in file:
            continue

        res = download_file(file, course_name, sheet)
        if res:
            upload_drive(res, course_name)
        elif res == None:
            print('Error: {}'.format(url))

def fetch_splash(course, root):
    sheets = root.xpath('//li[contains(@class,"modtype_assign")]//span[@class="instancename"]/text()')
    sheets[:] = [re.search(course['pattern'], sheet).group(1).replace(' ', '').lower().translate(umlaut_map) for sheet in sheets]

    assignments = root.xpath('//li[contains(@class,"modtype_assign")]//a/@href')
    for pair in zip(sheets, assignments):
        fetch_assignment(course['name'], pair[0], pair[1])

def fetch(course, root, script=False):
    if script:
        pattern = course['script']
        folder = 'scripts'
    else:
        pattern = course['pattern']
        folder = 'assignments'

    files = regex_xpath(root, '//span[@class="instancename" and re:match(text(),"{}")]/parent::a/@href'.format(pattern))
    for file in files:
        res = download_file(file, course['name'], folder=folder)
        if res:
            upload_drive(res, course['name'], folder=folder)
        elif res == None:
            print('Error: {}'.format(url))

login()

for course in COURSES:
    html = get('course/view.php?id={}'.format(course['id'])).text
    root = lxml.html.fromstring(html.encode('utf-8'))

    if 'script' in course:
        fetch(course, root, True)

    if course['splash']:
        fetch_splash(course, root)
    else:
        fetch(course, root)
